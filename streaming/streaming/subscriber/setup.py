
import setuptools

REQUIRED_PACKAGES = [
    "apache-beam[gcp]",
    "fsspec",
    "gcsfs",
    "loguru",
]

setuptools.setup(
    name="twitchstreaming",
    version="0.0.1",
    install_requires=REQUIRED_PACKAGES,
    packages=setuptools.find_packages(),
    include_package_data=True,
    description="Troll detection Streaming",
)

